package no.uib.inf101.datastructure;

import java.util.ArrayList;
import java.util.List;

public class TextGrid implements ITextGrid {

	private final List<List<String>> data;
	private final int cols;
	private final int rows;

	public TextGrid(int rows, int cols, String defaultString) {
		this.rows = rows;
		this.cols = cols;
		this.data = new ArrayList<List<String>>(rows);

		for (int i = 0; i < rows; i++) {
			List<String> row = new ArrayList<>(cols);
			for (int j = 0; j < cols; j++) {
				row.add(defaultString);
			}
			this.data.add(row);
		}
	}

	public TextGrid(int rows, int cols) {
		this(rows, cols, null);
	}

	@Override
	public int rows() {
		return rows;
	}

	@Override
	public int cols() {
		return cols;
	}

	@Override
	public List<CellText> getCells() {
		ArrayList<CellText> cells = new ArrayList<CellText>();

		for (int row = 0; row < rows; row++) {
			for (int col = 0; col < cols; col++) {
				CellPosition pos = new CellPosition(row, col);
				cells.add(new CellText(pos, this.get(pos)));
			}
		}

		return cells;
	}

	@Override
	public String get(CellPosition pos) {
		return this.data.get(pos.row()).get(pos.col());
	}

	@Override
	public void set(CellPosition pos, String text) {
		this.data.get(pos.row()).set(pos.col(), text);
	}
}
