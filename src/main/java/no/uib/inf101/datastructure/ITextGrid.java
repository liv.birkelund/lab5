package no.uib.inf101.datastructure;

public interface ITextGrid extends GridDimension, CellTextCollection {

	/**
	 * Get the text of the cell at the given position.
	 *
	 * @param pos the position
	 * @return the text of the cell
	 * @throws IndexOutOfBoundsException if the position is out of bounds
	 */
	String get(CellPosition pos);

	/**
	 * Set the text of the cell at the given position.
	 *
	 * @param pos  the position
	 * @param text the new text
	 * @throws IndexOutOfBoundsException if the position is out of bounds
	 */
	void set(CellPosition pos, String text);

}
