package no.uib.inf101.datastructure;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class ColorGrid implements IColorGrid {

	private final List<List<Color>> data;
	private final int cols;
	private final int rows;

	public ColorGrid(int rows, int cols, Color defaultColor) {
	    this.rows = rows;
	    this.cols = cols;
	    this.data = new ArrayList<List<Color>>(rows);
	    
	    for (int i = 0; i < rows; i++) {
	      List<Color> row = new ArrayList<>(cols);
	      for (int j = 0; j < cols; j++) {
	        row.add(defaultColor);
	      }
	      this.data.add(row);
	    }
	  }

	public ColorGrid(int rows, int cols) {
	    this(rows, cols, null);
	  }

	@Override
	public int rows() {
		return rows;
	}

	@Override
	public int cols() {
		return cols;
	}

	@Override
	public List<CellColor> getCells() {
		ArrayList<CellColor> cells = new ArrayList<CellColor>();

		for (int row = 0; row < rows; row++) {
			for (int col = 0; col < cols; col++) {
				CellPosition pos = new CellPosition(row, col);
				cells.add(new CellColor(pos, this.get(pos)));
			}
		}

		return cells;
	}

	@Override
	public Color get(CellPosition pos) {
		return this.data.get(pos.row()).get(pos.col());
	}

	@Override
	public void set(CellPosition pos, Color color) {
		this.data.get(pos.row()).set(pos.col(), color);
	}
}
