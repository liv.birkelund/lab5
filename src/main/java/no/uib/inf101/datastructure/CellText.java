package no.uib.inf101.datastructure;

// Les om records her: https://inf101.ii.uib.no/notat/mutabilitet/#record

/**
 * A CellText contains a CellPosition and a String.
 *
 * @param cellPosition  the position of the cell
 * @param text        the text of the cell
 */
public record CellText(CellPosition pos, String text) {
	
}
